package me.jy89hui.Checker;


import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class HomeWorkGUI extends BasicGame{
	public static final int HEIGHT = 1500;
	public static final int WIDTH = 900;


	public HomeWorkGUI(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	public static void LoadAndStart(){
		try{
		AppGameContainer app = new AppGameContainer(new HomeWorkGUI("My HomeWork:"));
		app.setDisplayMode(HEIGHT, WIDTH, false);
		app.setAlwaysRender(true);
		app.setShowFPS(false);
		for (int currentPage=0; currentPage<Main.currentPages.length; currentPage++){
			Bounds bounds = new Bounds(30, currentPage*75+30, 1000, 50);
			Main.currentPages[currentPage].assignBounds(bounds);
		}
		app.start();
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	@Override
	public void render(GameContainer arg0, Graphics g) throws SlickException {
		g.setBackground(new Color(110,163,181));
		if (Main.currentPages!=null){
			for (int currentPage=0; currentPage<Main.currentPages.length; currentPage++){
				Main.currentPages[currentPage].draw(g);
			}
		}
		
	}

	@Override
	public void init(GameContainer arg0) throws SlickException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(GameContainer arg0, int arg1) throws SlickException {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseClicked(int button, int locx, int locy, int clickCount){
		for (PageToCheck ptc : Main.currentPages){
			if (ptc.bounds.isInside(locx, locy)){
				try {
					java.awt.Desktop.getDesktop().browse(new URI(ptc.URL));
				} catch (IOException | URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	@Override
	public void keyPressed(int key, char c){
		if (key==Input.KEY_S){
			Main.fileManager.saveTheFile(Main.fileName);
			Main.log("Saved");
		}
	}

}
