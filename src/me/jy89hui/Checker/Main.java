package me.jy89hui.Checker;

public class Main {
	public static final String fileName = "HomeworkData.txt";
	public static final PageToCheck[] pagesWhenCreatingANewFile = new PageToCheck[]{
		new PageToCheck("Humanities (Adina)√false√###√http://teachers.northwestschool.org/nws/ameyer/9th-grade-homework-and-announcements√nothing"),
		new PageToCheck("Math (Alex)√false√###√http://teachers.northwestschool.org/nws/alex-chen/2nd-year-algebra-hw-assingments-10th-grade√nothing"),
		new PageToCheck("French (Francoise)√false√###√http://teachers.northwestschool.org/nws/fcanter/homework√nothing"),
		new PageToCheck("Drawing (Curtis)√false√###√http://teachers.northwestschool.org/nws/cerlinger/homework√nothing"),
		new PageToCheck("Should always be new√false√###√https://www.reddit.com/r/all/new/√nothing"),
		new PageToCheck("Apple.com√false√###√http://www.apple.com/√nothing")
	};
	public static PageToCheck[] currentPages;
	public static FileManager fileManager = new FileManager();
	public static void main(String[] args){
		
		log("Started");
		log("LoadingHomeworkData");
		currentPages = fileManager.loadDataFile(fileName);
		HomeWorkGUI.LoadAndStart();
		
	}
	public static void log(String s){
		System.out.println("[Log] "+s);
	}
}
