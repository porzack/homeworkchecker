package me.jy89hui.Checker;

public class Bounds {
	public int x,y,width,height;
	public Bounds(int x, int y, int width, int height){
		this.x=x;
		this.y=y;
		this.width=width;
		this.height=height;
	}
	public boolean isInside(int locX, int locY){
		if (locX>x && locY>y){
			if(locX<(x+width) && locY<(y+height)){
				return true;
			}
		}
		return false;
	}
}
