package me.jy89hui.Checker;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Date;

public class FileManager {
	public PageToCheck[] loadDataFile(String fileName){
		String line = null;
		PageToCheck[] pages = null;
        try {
            FileReader fileReader = 
                new FileReader(fileName);
            BufferedReader bufferedReader = 
                new BufferedReader(fileReader);
            while((line = bufferedReader.readLine()) != null) {
                pages = parseLineOfDataFile(line);
            }   
            bufferedReader.close();    
        }
        catch(FileNotFoundException ex) {
        	Main.log("Could Not Find File!");
            return Main.pagesWhenCreatingANewFile;
            
        }
        catch(IOException ex) {
            System.out.println(
                "Error reading file '" 
                + fileName + "'");
        }
		return pages;
	}
	private PageToCheck[] parseLineOfDataFile(String line){
		String[] pageData = line.split("∫");
		PageToCheck[] pages = new PageToCheck[pageData.length];
		for(int loc=0; loc<pageData.length; loc++){
			pages[loc] = new PageToCheck(pageData[loc]);
		}
		return pages;
	}
	public void saveTheFile(String fileName){
		String toWrite = "";
		for (int loc = 0; loc <Main.currentPages.length; loc++){
			PageToCheck ptc = Main.currentPages[loc];
			if (loc!=0){
				toWrite=toWrite+"∫";
			}
			toWrite=toWrite+ptc.getFullDataString();
		}
		try{
		FileWriter fw = new FileWriter(fileName);
		 
			fw.write(toWrite);
	 
		fw.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static String getHTMLFrom(String URL){
		String inputLine="";
		try{
		 	URL theurl = new URL(URL);
	        BufferedReader in = new BufferedReader(
	        new InputStreamReader(theurl.openStream()));
	        String makeshift="";
	        while ((makeshift=in.readLine()) != null){
	        	inputLine=inputLine+makeshift;
	        }
	        in.close();
	        return inputLine;
		}catch(Exception E){
			System.out.println("There was a problem.");
			E.printStackTrace();
		}
		return inputLine;
	}
}
