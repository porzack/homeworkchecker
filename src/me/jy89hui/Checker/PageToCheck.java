package me.jy89hui.Checker;

import java.util.Date;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class PageToCheck {
	/**
	 * FORMAT FOR dataInFile:
	 * ∫NameOfPageToCheck√[bool] hasBeenCheckedSinceChange√Date TimeOfCheck√URL√String HTML
	 * Note: The ∫ is cut off when split
	 */
	public Bounds bounds;
	public String name;
	public boolean shouldBeChecked;
	public boolean hasChanged;
	public String lastHTML;
	public String URL;
	public String currentHTML;
	public boolean hasBeenChecked;
	public String timeOfLastCheck;
	
	public PageToCheck(String dataInFile){
		String[] splitData = dataInFile.split("√");
		if (splitData.length!=5){
			throw new RuntimeException("Cannot load: "+dataInFile);
		}
		this.name=splitData[0];
		this.hasBeenChecked = Boolean.getBoolean(splitData[1]);
		this.timeOfLastCheck = splitData[2];
		this.URL = splitData[3];
		this.lastHTML=splitData[4];
		String currentHTML = FileManager.getHTMLFrom(this.URL);
		this.currentHTML=currentHTML;
		if (!this.lastHTML.equals(this.currentHTML)){
			this.hasChanged=true;
		}
		if (this.hasChanged  ||  (!this.hasBeenChecked)){
			this.shouldBeChecked=true;
		}
	}
	public String getFullDataString(){
		String data = "";
		data+=this.name;
		data+="√";
		data+=this.hasBeenChecked;
		data+="√";
		data+=((new Date()).toString());
		data+="√";
		data+=this.URL;
		data+="√";
		data+=this.currentHTML;
		return data;
	}
	public String toString(){
		return "Name: "+name+ " HasChanged: "+hasChanged+" HasBeenChecked: "+hasBeenChecked+" TimeOfCheck: "+timeOfLastCheck+" URL: "+URL+" CURRENT HTML: "+currentHTML;
	}
	public void HasChecked(){
		this.hasBeenChecked=true;
	}
	public void assignBounds(Bounds bound){
		this.bounds=bound;
	}
	public void draw(Graphics g){	
		g.setColor(new Color(30,30,30));
		g.fillRoundRect(bounds.x, bounds.y, bounds.width, bounds.height, 10);
		g.setColor(new Color(210,210,210));
		g.drawString(""+name, bounds.x, bounds.y+(bounds.height/2));
		if (hasChanged){
			g.setColor(new Color(200,50,50));
		}else{
			g.setColor(new Color(50,200,50));
		}
		g.drawString(""+hasChanged, bounds.width-100, bounds.y+(bounds.height/2));
	}
	
}






